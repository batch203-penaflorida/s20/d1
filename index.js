// function greeting() {
//   console.log("Hello World");
// }
// greeting() * 10;

// let count = 10;
// while (count !== 0) {
//   console.log("This is printed inside the loop." + count);
//   greeting();
//   count--;
// }

// While Loop

// while loop allows us to repeat an action or an instruction as long as condition is true.
/* 
   Syntax:
   while(expression/condition){
      //statement/code block
      finalExpression ++/-- (iteration)
   }
   - expression/condition => This are the unit of code that is being evaluated in our loop. Loop will run while the condition/expression is true.
   -statement/code block => code/instructions that will be executed several times.
   - finalExpression => indicates how to advance the loop.
*/

// let count = 5;

// while (count !== 0) {
//   console.log(
//     "Current value of count in the condition: " + count + " !==0 is true"
//   );

//   //Forgetting to include this in loop will make our application run an infinite loop which will eventually crash our devices.
//   count--;
// }

/* 
   A do while works a lot like the loop while
   do{
      statement/ code block
      final expression
   }
   while(expression/condition)
*/

// let number = Number(prompt("Give me a number"));

// do {
//   console.log("Do While: " + number);
//   //increase the value of number by 1 after every iteration to stop.101012312321321
//   number++;
// } while (number <= 10);

// while (number < 10) {
//   console.log("While: " + number);
//   number++;
// }

/*
while(not edge){
   run
} 
*/

/* do {
   run();
}
while(not edge)
 */

// [SECTION] For Loop
// A for loop is more flexible than while and do-while loops
/* 
   Syntax:
      for(initialization; expression/condition; finalexpression++/--){
      
      }
      Three parts of the for loop:
      1. initialization => value that will track the progression of the loop.
      2. expression/condition => This will be evaluated to determine if the loop will run one more time.
      3. Final Expression/condition => indicates how the loop advances.
*/
// Main

// for (let count = 0; count <= 20; count++) {
//   if (count % 2 === 0 && count !== 0) {
//     console.log(`For Loop: ${count}`);
//   }
// }

/* 
      let myString = "Angelito";
      
*/
// Loop using string property

// length property is used to count the number of characters in a string.
// let myString = "Angelito";
// for (let i = 0; i < myString.length; i++) {
//   console.log(myString[i]);
// }
// accessing element of a string

// individual characters of a string may be accessed using it's index number.
// the first character in string corresponds to the number 0, to the nth number.
// console.log(myString.length);
// console.log(myString[0]);
// console.log(myString[3]);
// console.log(myString[myString.length - 1]);

/* Create a loop that will print out the letters of the name individually. and print out the number 3 instead of the letter to be printed out is a vowel. */
// let myName = "Tolits";
// let myNewName = "";
// for (let i = 0; i < myName.length; i++) {
// console.log(str[i].toLowerCase());
// If the character of yourname is a vowel letter, instead of the character it will display 3.
//   if (
//     myName[i].toLowerCase() === "a" ||
//     myName[i].toLowerCase() === "e" ||
//     myName[i].toLowerCase() === "o" ||
//     myName[i].toLowerCase() === "i" ||
//     myName[i].toLowerCase === "u"
//   ) {
//     console.log("3");
//     myNewName += 3;
//   } else {
//     //consonant
//     console.log(myName[i]);
//     myNewName += myName[i];
//   }
// }

// console.log(myNewName);

// Continue and Break Statements

/* "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all the code block.
   "break" statement is used to terminate the current loop once a match has been found.
*/

/* 
   Create a loop that if the count values is divided by 2 and the remainder is 0, it will not print the number and contine to the next iteration of the loop. If the count value is less than 10 it will stop the loop.
*/
for (let count = 0; count <= 20; count++) {
  //   console.log(count);
  if (count % 2 === 0) {
    continue;
    // This ignores
    //  console.log("Even: " + count);
  }
  console.log("Continue and break: " + count);
  if (count > 10) {
    break;
  }
}
